# Caesar 3 PT-BR Steam Patch

[Clique aqui para ler esse documento em Português.](./README.md)

This patch applies the official PT-BR localization, including speeches, to the Caesar 3 game from Steam.

## Install

1. Download the patch through [this link](https://gitlab.com/felipecrs/caesar3-ptbr-steam-patch/-/archive/main/caesar3-ptbr-steam-patch-main.zip)
2. Extract it to any folder
3. Simply copy all the files and folders from the [Caesar 3](./Caesar%203/) folder to where your Caesar 3 from Steam is installed, replacing them when asked.

## Features

- [Julius](https://github.com/bvschaik/julius) 1.7.0
- The [`pt-br`](./Caesar%203/C3/pt-br/) language pack [to be used with Julius](https://github.com/bvschaik/julius/wiki/Configuration#language-packs). See [Language pack](#language-pack) for more details.
- The [`julius.ini`](./Caesar%203/C3/julius.ini), configured to use the pt-br language pack and to play the intro videos in the initialization.
- The [`SierraLauncher.ini`](./Caesar%203/SierraLauncher.ini), configured to execute the [`julius.exe`](./Caesar%203/C3/julius.exe) rather than `c3.exe`, so the game launcher will open Julius.
- The original pt-br [`MANUAL.PDF`](./Caesar%203/C3/pt-br/MANUAL.PDF) in the language pack folder
- The high quality `.mp3` files for the cities music taken from the [Julius GitHub wiki](https://github.com/bvschaik/julius/wiki/MP3-Support).

### Language pack

It was made using the following procedure:

1. Installed the game using the official PT-BR CD
2. Applied the [official v1.1 pt-br patch](https://github.com/bvschaik/julius/issues/566)
3. Taken the `smk\` and `wavs\` folders and the `c3.eng` and `c3_mm.eng` files from the installation directory
4. Copied the `SMK\Intro.smk` file from the CD to the `smk\` folder (it's was there because the original game reads it from the CD to preserve disk space)
5. Renamed the `wavs\vigils_exact0.wav` file to `wavs\vigils_exact10.wav` (it was probably an error when the PT-BR version of the game was created, as no speech file starts with 0 and there is a `vigils_exact10.wav` present in the English version)
6. Removed all the files which does not contain speeches, since Julius can read those from the already existing files. This is the list of files removed:

    <details>
      <summary>Click to expand</summary>

    ```sh-session
    ❯ git diff-tree -r --name-only --diff-filter=D v1 v2 | sed -e 's/^Caesar 3\/C3\/pt-br\///' | tree --fromfile=stdin
    .
    ├── smk
    │   ├── 1ST_GLAD.SMK
    │   ├── 1st_Chariot.smk
    │   ├── ARMY_WIN.SMK
    │   ├── Army_Lose.smk
    │   ├── Emmigrate.smk
    │   ├── Empire_Contract.smk
    │   ├── Empire_Expand.smk
    │   ├── Festival2_chariot.smk
    │   ├── Festival3_Glad.smk
    │   ├── GOD_MARS.SMK
    │   ├── Glad_revolt.smk
    │   ├── God_Ceres.smk
    │   ├── God_Mercury.smk
    │   ├── God_Neptune.smk
    │   ├── God_Venus.smk
    │   ├── Lose_game.smk
    │   ├── Mugging.smk
    │   ├── Population1.smk
    │   ├── Population2.smk
    │   ├── Population3.smk
    │   ├── QUAKE.SMK
    │   ├── RIOT.SMK
    │   ├── SICK.SMK
    │   ├── SPY_ARMY.SMK
    │   ├── SPY_RIOT.SMK
    │   ├── Spy_Barbarian.smk
    │   ├── Urgent_message1.smk
    │   ├── Urgent_message2.smk
    │   ├── Victory_Balcony.smk
    │   ├── Victory_Senate.smk
    │   ├── WIN_GAME.SMK
    │   ├── city_Fire.smk
    │   └── festival1_feast.smk
    └── wavs
        ├── ACADEMY.WAV
        ├── AQUADUCT.WAV
        ├── ARROW.WAV
        ├── ART_PIT.WAV
        ├── AXE.WAV
        ├── Army_war_cry.wav
        ├── BALLISTA.WAV
        ├── BARBER.WAV
        ├── BARRACKS.WAV
        ├── BATHS.WAV
        ├── BUILD1.WAV
        ├── CAMEL1.WAV
        ├── CAMEL2.WAV
        ├── CHAR_PIT.WAV
        ├── CLAY.WAV
        ├── CLAY_PIT.WAV
        ├── CLINIC.WAV
        ├── CLUB.WAV
        ├── COHORT.WAV
        ├── COHORT1.WAV
        ├── COHORT2.WAV
        ├── COHORT3.WAV
        ├── COHORT4.WAV
        ├── COHORT5.WAV
        ├── COIN.WAV
        ├── Combat_Long.wav
        ├── Combat_Short.wav
        ├── DIE1.WAV
        ├── DIE10.WAV
        ├── DIE11.WAV
        ├── DIE2.WAV
        ├── DIE3.WAV
        ├── DIE4.WAV
        ├── DIE5.WAV
        ├── DIE6.WAV
        ├── DIE7.WAV
        ├── DIE8.WAV
        ├── DIE9.WAV
        ├── DOCK.WAV
        ├── DOCK1.WAV
        ├── DOCK2.WAV
        ├── DRUMS.WAV
        ├── ELEPHANT.WAV
        ├── EXPLO1.WAV
        ├── EXPLOD1.WAV
        ├── Empty_land1.wav
        ├── Empty_land2.wav
        ├── Empty_land3.wav
        ├── Empty_land4.wav
        ├── FANFARE.WAV
        ├── FANFARE2.WAV
        ├── FORT.WAV
        ├── FORT1.WAV
        ├── FORT2.WAV
        ├── FORT3.WAV
        ├── FORT4.WAV
        ├── FORUM.WAV
        ├── FOUNTAIN.WAV
        ├── Fire_splash.wav
        ├── GARDENS1.WAV
        ├── GARDENS2.WAV
        ├── GARDENS3.WAV
        ├── GARDENS4.WAV
        ├── GLAD_PIT.WAV
        ├── GRANARY.WAV
        ├── GRANARY1.WAV
        ├── GRANARY2.WAV
        ├── HORN1.WAV
        ├── HORN2.WAV
        ├── HORN3.WAV
        ├── HORN4.WAV
        ├── HORSE.WAV
        ├── HORSE2.WAV
        ├── HOSPITAL.WAV
        ├── HOUSING.WAV
        ├── ICON1.WAV
        ├── JAVELIN.WAV
        ├── LIBRARY.WAV
        ├── LION_DIE.WAV
        ├── LION_PIT.WAV
        ├── MARCHING.WAV
        ├── MARKET.WAV
        ├── MARKET1.WAV
        ├── MARKET2.WAV
        ├── MARKET3.WAV
        ├── MARKET4.WAV
        ├── MIL_ACAD.WAV
        ├── MINE.WAV
        ├── ORACLE.WAV
        ├── PALACE.WAV
        ├── PANEL1.WAV
        ├── PANEL2.WAV
        ├── PANEL3.WAV
        ├── PARK.WAV
        ├── QUARRY.WAV
        ├── RESEVOIR.WAV
        ├── RIVER.WAV
        ├── ROME1.WAV
        ├── ROME2.WAV
        ├── ROME3.WAV
        ├── ROME4.WAV
        ├── ROME5.WAV
        ├── RUIN.WAV
        ├── SCHOOL.WAV
        ├── SENATE.WAV
        ├── SETUP.WAV
        ├── SHIPYARD.WAV
        ├── STATUE.WAV
        ├── SWORD.WAV
        ├── TEMP_WAR.WAV
        ├── THEATRE.WAV
        ├── TIMBER.WAV
        ├── TOWER.WAV
        ├── TOWER1.WAV
        ├── TOWER2.WAV
        ├── TOWER3.WAV
        ├── TOWER4.WAV
        ├── VEG_FARM.WAV
        ├── WELL.WAV
        ├── WHARF.WAV
        ├── WHARF1.WAV
        ├── WHARF2.WAV
        ├── WHEAT.WAV
        ├── WOLF_DIE.WAV
        ├── WinAssignment.wav
        ├── ampitheatre.wav
        ├── arrow_hit.wav
        ├── ballista_hit_ground.wav
        ├── ballista_hit_person.wav
        ├── ballista_hit_person2.wav
        ├── barbarian_war_cry.wav
        ├── burning_ruin.wav
        ├── colloseum.wav
        ├── elephant_die.wav
        ├── elephant_hit.wav
        ├── elephant_hit2.wav
        ├── empty_land.wav
        ├── fanfare_nu1.wav
        ├── fanfare_nu2.wav
        ├── fanfare_nu3.wav
        ├── fanfare_nu4.wav
        ├── fanfare_nu5.wav
        ├── figs_farm.wav
        ├── formation_shield.wav
        ├── furn_workshop.wav
        ├── furniture_workshop.wav
        ├── gov_palace.wav
        ├── hippodrome.wav
        ├── horse_mov.wav
        ├── house_good1.wav
        ├── house_good2.wav
        ├── house_good3.wav
        ├── house_good4.wav
        ├── house_mid1.wav
        ├── house_mid2.wav
        ├── house_mid3.wav
        ├── house_mid4.wav
        ├── house_poor1.wav
        ├── house_poor2.wav
        ├── house_poor3.wav
        ├── house_poor4.wav
        ├── house_posh1.wav
        ├── house_posh2.wav
        ├── house_posh3.wav
        ├── house_posh4.wav
        ├── house_slum1.wav
        ├── house_slum2.wav
        ├── house_slum3.wav
        ├── house_slum4.wav
        ├── lion_attack.wav
        ├── lumber_mill.wav
        ├── meat_farm.wav
        ├── oil_workshop.wav
        ├── olives_farm.wav
        ├── pott_workshop.wav
        ├── pottery_workshop.wav
        ├── quake_explode.wav
        ├── quake_rumble.wav
        ├── sheep_baa.wav
        ├── sheep_die.wav
        ├── shipyard1.wav
        ├── shipyard2.wav
        ├── spear_attack.wav
        ├── sword_light.wav
        ├── sword_swing.wav
        ├── temp_comm.wav
        ├── temp_farm.wav
        ├── temp_love.wav
        ├── temp_ship.wav
        ├── temple_comm.wav
        ├── temple_farm.wav
        ├── temple_love.wav
        ├── temple_ship.wav
        ├── temple_war.wav
        ├── vines_farm.wav
        ├── warehouse.wav
        ├── warehouse1.wav
        ├── warehouse2.wav
        ├── weap_workshop.wav
        ├── weapons_workshop.wav
        ├── wheat_farm.wav
        ├── wine_workshop.wav
        ├── wolf_attack.wav
        ├── wolf_attack2.wav
        ├── wolf_attack3.wav
        ├── wolf_attack4.wav
        ├── wolf_attack5.wav
        ├── wolf_howl.wav
        └── zebra_die.wav

    2 directories, 242 files
    ```

    </details>
7. Added the merchant speeches (of boat and donkey) which were left out in the official localization of the game, recorded by [Horieber Oliveira](https://www.youtube.com/channel/UCUoEqh3nqgqXz4pjqx0quSQ). See the [introducing MR](https://gitlab.com/felipecrs/caesar3-ptbr-steam-patch/-/merge_requests/1) for more information on how the speeches were edited, and also in case you have any idea to enhance the samples. They are in total 10 speeches:

    <details>
      <summary>Click to expand</summary>

    - [boats_exact1.wav](./Caesar%203/C3/pt-br/wavs/boats_exact1.wav)
    - [boats_exact2.wav](./Caesar%203/C3/pt-br/wavs/boats_exact2.wav)
    - [boats_exact3.wav](./Caesar%203/C3/pt-br/wavs/boats_exact3.wav)
    - [boats_exact4.wav](./Caesar%203/C3/pt-br/wavs/boats_exact4.wav)
    - [boats_exact5.wav](./Caesar%203/C3/pt-br/wavs/boats_exact5.wav)
    - [donkey_exact1.wav](./Caesar%203/C3/pt-br/wavs/donkey_exact1.wav)
    - [donkey_exact2.wav](./Caesar%203/C3/pt-br/wavs/donkey_exact2.wav)
    - [donkey_exact3.wav](./Caesar%203/C3/pt-br/wavs/donkey_exact3.wav)
    - [donkey_exact4.wav](./Caesar%203/C3/pt-br/wavs/donkey_exact4.wav)
    - [donkey_exact5.wav](./Caesar%203/C3/pt-br/wavs/donkey_exact5.wav).

    </details>

### Credits

All the files inside the folder [`pt-br`](./Caesar%203/C3/pt-br/), except for the ones which starts with `boats` and `donkey` in the `wavs` folder, were taken from the original Caesar 3 disk. All the rights are from the [Brasoft](https://pt.wikipedia.org/wiki/Brasoft) company, which created the original localization of the game to the PT-BR language.

The merchant speeches (of boat and donkey) were dubbed voluntarily by Horieber Oliveira, and you can check out more of his awesome job in his [YouTube channel](https://www.youtube.com/channel/UCUoEqh3nqgqXz4pjqx0quSQ). Thanks to Henderson Souza who intermediated it, without him this would not happen.

### Original timestamps

For those who care, these are the original timestamps of the files:

<details>
  <summary>Click to expand</summary>

```sh-session
❯ tree -D --timefmt "%F %T"
.
├── [2000-10-30 21:25:38]  MANUAL.PDF
├── [1999-12-22 15:26:02]  c3.eng
├── [1999-12-20 13:08:46]  c3_mm.eng
├── [2021-01-10 13:17:20]  smk
│   ├── [1999-07-20 16:38:24]  CREDITS.SMK
│   ├── [1999-06-28 09:46:12]  Emp_2nd_chance.smk
│   ├── [1999-06-28 09:50:56]  Emp_angry.smk
│   ├── [1999-06-28 09:42:24]  Emp_displeased.smk
│   ├── [1999-06-28 09:49:02]  Emp_send_army.smk
│   ├── [1999-06-28 13:40:16]  Intro.smk
│   └── [1999-07-21 08:59:58]  Logo.smk
└── [2021-01-10 13:17:21]  wavs
    ├── [1999-06-30 08:35:14]  01B.WAV
    ├── [1999-05-18 09:48:08]  01W.WAV
    ├── [1999-06-30 08:36:00]  02B.WAV
    ├── [1999-05-18 09:48:38]  02W.WAV
    ├── [1999-07-07 12:48:36]  03B.WAV
    ├── [1999-05-18 09:49:48]  03W.WAV
    ├── [1999-07-07 08:24:26]  04B.WAV
    ├── [1999-05-18 09:51:24]  04W.WAV
    ├── [1999-05-18 09:52:00]  05B.WAV
    ├── [1999-05-18 09:52:08]  05W.WAV
    ├── [1999-05-18 09:52:40]  06B.WAV
    ├── [1999-05-18 09:52:48]  06W.WAV
    ├── [1999-06-30 08:39:12]  07B.WAV
    ├── [1999-05-18 09:53:42]  07W.WAV
    ├── [1999-06-30 08:39:58]  08B.WAV
    ├── [1999-05-18 09:54:22]  08W.WAV
    ├── [1999-07-07 08:25:10]  09B.WAV
    ├── [1999-05-18 09:55:10]  09W.WAV
    ├── [1999-05-18 09:56:26]  10B.WAV
    ├── [1999-05-18 09:56:32]  10W.WAV
    ├── [1999-05-18 09:57:04]  11B.WAV
    ├── [1999-05-18 09:57:12]  11W.WAV
    ├── [1999-05-18 09:57:36]  12B.WAV
    ├── [1999-05-18 09:57:44]  12W.WAV
    ├── [1999-06-30 08:40:34]  13B.WAV
    ├── [1999-05-18 09:58:14]  13W.WAV
    ├── [1999-06-30 08:41:06]  14B.WAV
    ├── [1999-06-30 08:41:14]  14W.WAV
    ├── [1999-05-18 09:59:24]  15B.WAV
    ├── [1999-05-18 09:59:32]  15W.WAV
    ├── [1999-05-18 09:59:48]  16B.WAV
    ├── [1999-05-18 09:59:56]  16W.WAV
    ├── [1999-05-18 10:00:20]  17B.WAV
    ├── [1999-05-18 10:00:26]  17W.WAV
    ├── [1999-05-18 10:00:52]  18B.WAV
    ├── [1999-05-18 10:01:00]  18W.WAV
    ├── [1999-05-18 10:01:36]  19B.WAV
    ├── [1999-07-14 07:55:56]  19W.WAV
    ├── [1999-05-18 10:02:04]  20B.WAV
    ├── [1999-07-14 07:55:56]  20W.WAV
    ├── [1999-05-31 06:32:42]  Taxman_needjob1.WAV
    ├── [1999-05-31 06:36:42]  actors_great1.wav
    ├── [1999-05-31 06:36:44]  actors_great2.wav
    ├── [1999-05-31 06:36:46]  actors_needjob1.wav
    ├── [1999-05-31 06:36:48]  actors_nofun1.wav
    ├── [1999-05-31 06:36:50]  actors_nojob1.wav
    ├── [1999-05-31 06:36:54]  actors_relig1.wav
    ├── [1999-05-31 06:36:56]  actors_starv1.wav
    ├── [1999-05-31 06:34:56]  barber_great1.wav
    ├── [1999-05-31 06:34:58]  barber_great2.wav
    ├── [1999-05-31 06:35:00]  barber_needjob1.wav
    ├── [1999-05-31 06:35:02]  barber_nofun1.wav
    ├── [1999-05-31 06:35:04]  barber_nojob1.wav
    ├── [1999-05-31 06:35:06]  barber_relig1.wav
    ├── [1999-05-31 06:35:08]  barber_starv1.wav
    ├── [1999-05-31 06:31:56]  bather_great1.wav
    ├── [1999-05-31 06:31:58]  bather_great2.wav
    ├── [1999-05-31 06:32:00]  bather_needjob1.wav
    ├── [1999-05-31 06:32:02]  bather_nofun1.wav
    ├── [1999-05-31 06:32:04]  bather_nojob1.wav
    ├── [1999-05-31 06:32:06]  bather_relig1.wav
    ├── [1999-05-31 06:32:08]  bather_starv1.wav
    ├── [1999-05-31 06:31:02]  charot_great1.wav
    ├── [1999-05-31 06:31:04]  charot_great2.wav
    ├── [1999-05-31 06:31:06]  charot_needjob1.wav
    ├── [1999-05-31 06:31:08]  charot_nofun1.wav
    ├── [1999-05-31 06:31:10]  charot_nojob1.wav
    ├── [1999-05-31 06:31:12]  charot_relig1.wav
    ├── [1999-05-31 06:31:14]  charot_starv1.wav
    ├── [1999-07-07 08:25:42]  crtpsh_exact1.wav
    ├── [1999-05-31 06:32:12]  crtpsh_exact2.wav
    ├── [1999-07-07 08:25:40]  crtpsh_exact3.wav
    ├── [1999-05-31 06:32:16]  crtpsh_great1.wav
    ├── [1999-07-07 08:25:38]  crtpsh_great2.wav
    ├── [1999-07-07 08:25:34]  crtpsh_needjob1.wav
    ├── [1999-07-07 08:25:36]  crtpsh_nofun1.WAV
    ├── [1999-07-07 08:25:44]  crtpsh_nojob1.wav
    ├── [1999-07-05 07:53:40]  crtpsh_relig1.wav
    ├── [1999-06-30 15:48:08]  crtpsh_starv1.wav
    ├── [1999-05-31 06:34:18]  doctor_great1.wav
    ├── [1999-05-31 06:34:20]  doctor_great2.wav
    ├── [1999-05-31 06:34:22]  doctor_needjob1.wav
    ├── [1999-05-31 06:34:24]  doctor_nofun1.wav
    ├── [1999-05-31 06:34:26]  doctor_nojob1.wav
    ├── [1999-05-31 06:34:28]  doctor_relig1.wav
    ├── [1999-05-31 06:34:30]  doctor_starv1.wav
    ├── [1999-05-31 06:33:56]  emigrate_exact1.wav
    ├── [1999-05-31 06:33:58]  emigrate_exact2.wav
    ├── [1999-05-31 06:34:00]  emigrate_exact3.wav
    ├── [1999-05-31 06:34:02]  emigrate_exact4.wav
    ├── [1999-07-14 07:55:48]  emigrate_exact5.WAV
    ├── [1999-05-31 06:32:54]  engine_exact1.wav
    ├── [1999-05-31 06:32:56]  engine_exact2.wav
    ├── [1999-05-31 06:32:58]  engine_great1.wav
    ├── [1999-05-31 06:32:58]  engine_great2.wav
    ├── [1999-05-31 06:33:00]  engine_needjob1.wav
    ├── [1999-05-31 06:33:04]  engine_nofun1.wav
    ├── [1999-05-31 06:33:06]  engine_nojob1.wav
    ├── [1999-05-31 06:33:08]  engine_relig1.wav
    ├── [1999-05-31 06:33:10]  engine_starv1.wav
    ├── [1999-05-31 06:36:58]  gladtr_exact1.wav
    ├── [1999-05-31 06:37:00]  gladtr_great1.wav
    ├── [1999-05-31 06:37:02]  gladtr_great2.wav
    ├── [1999-05-31 06:37:04]  gladtr_needjob1.wav
    ├── [1999-05-31 06:37:08]  gladtr_nofun1.wav
    ├── [1999-05-31 06:37:10]  gladtr_nojob1.wav
    ├── [1999-05-31 06:37:12]  gladtr_relig1.wav
    ├── [1999-05-31 06:37:14]  gladtr_starv1.wav
    ├── [1999-05-31 06:34:04]  granboy_exact1.wav
    ├── [1999-05-31 06:34:08]  granboy_exact2.wav
    ├── [1999-05-31 06:34:10]  granboy_exact3.wav
    ├── [1999-05-31 06:34:12]  homeless_exact1.WAV
    ├── [1999-05-31 06:34:14]  homeless_exact2.WAV
    ├── [1999-05-31 07:22:22]  immigrant_exact1.wav
    ├── [1999-05-31 07:22:22]  immigrant_exact2.wav
    ├── [1999-05-31 07:22:24]  immigrant_exact3.wav
    ├── [1999-05-31 06:34:32]  lionTr_exact1.wav
    ├── [1999-05-31 06:34:34]  lionTr_exact2.wav
    ├── [1999-05-31 06:34:36]  lionTr_exact3.wav
    ├── [1999-05-31 06:34:38]  lionTr_great1.wav
    ├── [1999-05-31 06:34:40]  lionTr_great2.wav
    ├── [1999-05-31 06:34:44]  lionTr_needjob1.wav
    ├── [1999-05-31 06:34:46]  lionTr_nofun1.wav
    ├── [1999-05-31 06:34:48]  lionTr_nojob1.wav
    ├── [1999-05-31 06:34:52]  lionTr_relig1.wav
    ├── [1999-05-31 06:34:54]  lionTr_starv1.wav
    ├── [1999-07-02 06:55:28]  lose_game.wav
    ├── [1999-05-31 06:35:46]  market_exact1.wav
    ├── [1999-05-31 06:35:46]  market_exact2.wav
    ├── [1999-06-07 13:51:04]  market_exact3.wav
    ├── [1999-05-31 06:35:48]  market_great1.wav
    ├── [1999-05-31 06:35:50]  market_great2.wav
    ├── [1999-05-31 06:35:52]  market_needjob1.wav
    ├── [1999-05-31 06:35:54]  market_nofun1.wav
    ├── [1999-05-31 06:35:56]  market_nojob1.wav
    ├── [1999-05-31 06:35:58]  market_relig1.wav
    ├── [1999-05-31 06:36:00]  market_starv1.wav
    ├── [1999-05-31 06:35:42]  mission_exact4.wav
    ├── [1999-05-31 06:35:36]  missionary_exact1.wav
    ├── [1999-05-31 06:35:38]  missionary_exact2.wav
    ├── [1999-05-31 06:35:32]  missionary_exact3.wav
    ├── [1999-05-31 06:36:14]  patric_great1.wav
    ├── [1999-05-31 06:36:16]  patric_great2.wav
    ├── [1999-05-31 06:36:18]  patric_needjob1.wav
    ├── [1999-05-31 06:36:20]  patric_nofun1.wav
    ├── [1999-05-31 06:36:22]  patric_nojob1.wav
    ├── [1999-05-31 06:36:24]  patric_relig1.wav
    ├── [1999-05-31 06:36:26]  patric_starv1.wav
    ├── [1999-05-31 06:36:28]  pleb_great1.wav
    ├── [1999-05-31 06:36:30]  pleb_great2.wav
    ├── [1999-05-31 06:36:32]  pleb_needjob1.wav
    ├── [1999-05-31 06:36:34]  pleb_nofun1.wav
    ├── [1999-05-31 06:36:36]  pleb_nojob1.wav
    ├── [1999-05-31 06:36:38]  pleb_relig1.wav
    ├── [1999-05-31 06:36:40]  pleb_starv1.wav
    ├── [1999-05-31 06:31:14]  priest_great1.wav
    ├── [1999-05-31 06:31:16]  priest_great2.wav
    ├── [1999-05-31 06:31:18]  priest_needjob1.wav
    ├── [1999-05-31 06:31:22]  priest_nofun1.wav
    ├── [1999-05-31 06:31:22]  priest_nojob1.wav
    ├── [1999-05-31 06:31:24]  priest_relig1.wav
    ├── [1999-05-31 06:31:26]  priest_starv1.wav
    ├── [1999-05-31 06:36:00]  pupils_great1.wav
    ├── [1999-05-31 06:36:02]  pupils_great2.wav
    ├── [1999-05-31 06:36:04]  pupils_needjob1.wav
    ├── [1999-05-31 06:36:06]  pupils_nofun1.wav
    ├── [1999-05-31 06:36:08]  pupils_nojob1.wav
    ├── [1999-05-31 06:36:10]  pupils_relig1.wav
    ├── [1999-05-31 06:36:10]  pupils_starv1.wav
    ├── [1999-05-31 06:31:50]  rioter_exact1.wav
    ├── [1999-05-31 06:31:52]  rioter_exact2.wav
    ├── [1999-05-31 06:31:56]  rioter_exact3.wav
    ├── [1999-05-31 06:32:32]  taxman_exact1.wav
    ├── [1999-05-31 06:32:34]  taxman_exact2.wav
    ├── [1999-05-31 06:32:38]  taxman_exact3.wav
    ├── [1999-05-31 06:32:40]  taxman_great1.wav
    ├── [1999-05-31 06:32:42]  taxman_great2.wav
    ├── [1999-05-31 06:32:46]  taxman_nofun1.wav
    ├── [1999-05-31 06:32:48]  taxman_nojob1.wav
    ├── [1999-05-31 06:32:50]  taxman_relig1.wav
    ├── [1999-05-31 06:32:52]  taxman_starv1.wav
    ├── [1999-05-31 06:35:10]  teach_great1.wav
    ├── [1999-05-31 06:35:12]  teach_great2.wav
    ├── [1999-05-31 06:35:14]  teach_needjob1.wav
    ├── [1999-05-31 06:35:16]  teach_nofun1.wav
    ├── [1999-05-31 06:35:18]  teach_nojob1.wav
    ├── [1999-05-31 06:35:20]  teach_relig1.wav
    ├── [1999-05-31 06:35:22]  teach_starv1.wav
    ├── [1999-05-31 06:34:14]  unemploy_exact1.wav
    ├── [1999-05-31 06:34:16]  unemploy_exact2.wav
    ├── [1999-05-31 06:33:12]  vigils_exact1.wav
    ├── [1999-05-31 06:33:14]  vigils_exact10.wav
    ├── [1999-05-31 06:33:16]  vigils_exact2.wav
    ├── [1999-05-31 06:33:16]  vigils_exact3.wav
    ├── [1999-05-31 06:33:18]  vigils_exact4.wav
    ├── [1999-05-31 06:33:20]  vigils_exact5.wav
    ├── [1999-05-31 06:33:22]  vigils_exact6.wav
    ├── [1999-05-31 06:33:22]  vigils_exact7.wav
    ├── [1999-05-31 06:33:24]  vigils_exact8.wav
    ├── [1999-05-31 06:33:26]  vigils_exact9.wav
    ├── [1999-05-31 06:33:28]  vigils_great1.wav
    ├── [1999-05-31 06:33:28]  vigils_great2.wav
    ├── [1999-06-07 13:51:06]  vigils_needjob1.wav
    ├── [1999-05-31 06:33:30]  vigils_nofun1.wav
    ├── [1999-05-31 06:33:32]  vigils_nojob1.wav
    ├── [1999-05-31 06:33:34]  vigils_relig1.wav
    ├── [1999-05-31 06:33:36]  vigils_starv1.wav
    ├── [1999-05-31 06:33:38]  wallguard_exact1.wav
    ├── [1999-05-31 06:33:38]  wallguard_exact2.wav
    ├── [1999-06-07 13:51:08]  wallguard_exact3.wav
    ├── [1999-05-31 06:33:40]  wallguard_exact4.wav
    ├── [1999-05-31 06:33:42]  wallguard_exact5.wav
    ├── [1999-05-31 06:33:44]  wallguard_great1.wav
    ├── [1999-05-31 06:33:46]  wallguard_great2.wav
    ├── [1999-05-31 06:33:48]  wallguard_needjob1.wav
    ├── [1999-05-31 06:33:50]  wallguard_nofun1.wav
    ├── [1999-05-31 06:33:52]  wallguard_nojob1.wav
    ├── [1999-05-31 06:33:54]  wallguard_relig1.wav
    └── [1999-05-31 06:33:54]  wallguard_starv1.wav

2 directories, 229 files
```

</details>
